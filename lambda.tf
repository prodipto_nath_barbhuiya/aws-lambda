resource "aws_lambda_function" "test_lambda" {
  function_name = "python-function"
  description   = "Example Python Lambda Function that returns an HTTP response."
  filename      = "main.py"
  runtime       = "python3.8"
  handler       = "main.lambda_handler"

  timeout     = 30
  memory_size = 128

  role_arn = aws_iam_role.lambda_role.arn

  
}


resource "aws_s3_bucket_notification" "my-trigger" {
    bucket = "pro-project"

    lambda_function {
        lambda_function_arn = "aws_lambda_function.test_lambda.arn"
        events              = ["s3:ObjectCreated:*"]
        filter_prefix       = "AWSLogs/"
        filter_suffix       = ".json"
    }
}

resource "aws_lambda_permission" "test" {
  statement_id  = "AllowS3Invoke"
  action        = "lambda:InvokeFunction"
  function_name = "aws_lambda_function.my-function.arn"
  principal = "s3.amazonaws.com"
  source_arn = "arn:aws:s3:::pro-project"
}
